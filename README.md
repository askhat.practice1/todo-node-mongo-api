# Description of the API endpoints for this TODO application:

### Account Manipulation
These endpoints do not require authentication

    - POST /api/v1/users/register - register a new user with email, password, first name, and last name
    - POST /api/v1/users/login - log in a user with email and password

### Task List Manipulation
These endpoints require authentication via JWT tokens in the Authorization header

    - POST /api/v1/tasks/ - create a new task with title and description for the authenticated user
    - GET /api/v1/tasks/ - get all tasks that are not completed for the authenticated user
    - GET /api/v1/tasks/completed/ - get all completed tasks that belong to the authenticated user

### Task List Manipulation
These endpoints require authentication via JWT tokens in the Authorization header

    - GET /api/v1/task/:taskId - get a specific task by taskId that is not completed and belongs to the authenticated user
    - PUT /api/v1/task/:taskId - update the title and/or description of a specific task by taskId that belongs to the authenticated user
    - PATCH /api/v1/task/:taskId/completed - mark a specific task by taskId as completed for the authenticated user
    - DELETE /api/v1/task/:taskId - delete a specific task by taskId that belongs to the authenticated user


    
