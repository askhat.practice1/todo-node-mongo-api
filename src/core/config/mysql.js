const mysql = require('mysql2/promise');

const dbConfig = {
  host: process.env.NODE_MYSQL_HOST || 'localhost',
  port: process.env.NODE_MYSQL_PORT || 3306,
  database: process.env.NODE_MYSQL_DATABASE || 'myapp',
  username: process.env.NODE_MYSQL_USERNAME || 'root',
  password: process.env.NODE_MYSQL_PASSWORD || 'root',
};

const dbClient = mysql.createPool({
  host: dbConfig.host,
  port: dbConfig.port,
  user: dbConfig.username,
  password: dbConfig.password,
  database: dbConfig.database,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

const db = {
  options: dbConfig,
  client: null,
};

const Sequelize = require("sequelize");
function connectToMySQL() {
  const sequelize = new Sequelize(dbConfig);
  sequelize.authenticate().then(() => {
    console.log("Connection has been established successfully.");
  }).catch(err => {
    console.error("Unable to connect to the database:", err);
    process.exit(1);
  });
  return sequelize;
}
const sequelize = connectToMySQL();

module.exports = sequelize;
