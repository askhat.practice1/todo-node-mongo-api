const databaseConfig = require("./database");
const jwtConfig = require("./jwt");
const mysqlConfig = require("./mysql");
const mongoConfig = require("./mongo");

module.exports = {
  database: databaseConfig,
  jwt: jwtConfig,
  mongo: mongoConfig,
  mysql: mysqlConfig,
};

