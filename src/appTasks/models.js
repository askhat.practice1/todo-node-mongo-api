const mongoose = require("mongoose");
const { UsersSchema } = require("../appUsers/models");

// const dbConfig = {
//     uri: process.env.NODE_MONGO_URI || "mongodb://localhost:27017/myapp",
//     options: {
//         useNewUrlParser: true,
//         useUnifiedTopology: true,
//         useCreateIndex: true,
//     },
// };

// mongoose
//     .connect(dbConfig.uri, dbConfig.options)
//     .then(() => {
//         console.log("Connection has been established successfully.");
//     })
//     .catch((err) => {
//         console.error("Unable to connect to the database:", err);
//         process.exit(1);
//     });

const TaskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    completed: {
        type: Boolean,
        required: true,
        default: false,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
    },
});

TaskSchema.index({ userId: 1 });

const TaskModel = mongoose.model("Task", TaskSchema);

module.exports = { TaskModel };
