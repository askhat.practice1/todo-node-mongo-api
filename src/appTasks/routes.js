const express = require("express");
const task_router = express.Router();
const tasks_router = express.Router();
const { logger, authenticateUser } = require("../middleware");

const { createTaskValidator, updateTaskValidator } = require("./validators");
const { TaskController, TasksController } = require("./controllers");

tasks_router.route("/")
    .get(logger, authenticateUser, TasksController.get_list_of_incompleted_tasks)
    .post(logger, authenticateUser, createTaskValidator, TasksController.post_task);

tasks_router.get("/completed/", logger, authenticateUser, TasksController.get_list_of_completed_tasks);

task_router.route("/:taskId/")
    .get(logger, authenticateUser, TaskController.get_task_by_taskId)
    .put(logger, authenticateUser, updateTaskValidator, TaskController.update_task_by_taskId)
    .patch(logger, authenticateUser, TaskController.complete_task_by_taskId)
    .delete(logger, authenticateUser, updateTaskValidator, TaskController.delete_task_by_taskId);

module.exports = { task_router, tasks_router };
