const TasksService = require("./services");
const { logger } = require("../utils/logger");
/* 
████████  █████  ███████ ██   ██ ███████ 
   ██    ██   ██ ██      ██  ██  ██      
   ██    ███████ ███████ █████   ███████ 
   ██    ██   ██      ██ ██  ██       ██ 
   ██    ██   ██ ███████ ██   ██ ███████ 
// */

async function post_task(req, res, next) {
  try {
    const task = await TasksService.create_task(
      req.user.id,
      req.body
    );
    res.status(201).json(task);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

async function get_list_of_incompleted_tasks(req, res, next) {
  try {
    const tasks = await TasksService.get_all_tasks(
      req.user.id,
      false
    );
    res.status(200).json(tasks);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

async function get_list_of_completed_tasks(req, res, next) {
  try {
    const tasks = await TasksService.get_all_tasks(
      req.user.id,
      true
    );
    res.status(200).json(tasks);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

const TasksController = {
  post_task,
  get_list_of_incompleted_tasks,
  get_list_of_completed_tasks,
}

/*
████████  █████  ███████ ██   ██ 
   ██    ██   ██ ██      ██  ██  
   ██    ███████ ███████ █████   
   ██    ██   ██      ██ ██  ██  
   ██    ██   ██ ███████ ██   ██ 
// */

async function get_task_by_taskId(req, res, next) {
  try {
    const task = await TasksService.get_task_by_id(
      req.user.id,
      req.params.taskId
    );
    res.status(200).json(task);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

async function update_task_by_taskId(req, res, next) {
  try {
    const task = await TasksService.update_task_by_id(
      req.user.id,
      req.params.taskId,
      req.body
    );
    res.json(task);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}
async function complete_task_by_taskId(req, res, next) {
  try {
    const task = await TasksService.complete_task_by_id(
      req.user.id,
      req.params.taskId
    );
    res.json(task);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

async function delete_task_by_taskId(req, res, next) {
  try {
    await TasksService.delete_task_by_id(
      req.user.id,
      req.params.taskId
    );
    res.sendStatus(204);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

const TaskController = {
  get_task_by_taskId,
  update_task_by_taskId,
  complete_task_by_taskId,
  delete_task_by_taskId,
};

module.exports = { TaskController, TasksController };
