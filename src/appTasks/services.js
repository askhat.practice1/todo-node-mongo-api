const { createCustomError } = require("../errors/custom-error");
const { TaskModel } = require("./models");

async function create_task(userId, { title, description }) {
    const existingTask = await TaskModel.findOne({ userId, title });
    if (existingTask) {
        throw createCustomError("A task with that title already exists", 400);
    }
    const task = await TaskModel.create({
        userId,
        title,
        description
    });
    return task;
}

async function get_all_tasks(userId, completed = false) {
    const tasks = await TaskModel.find({ userId, completed });
    return tasks;
}

async function get_task_by_id(userId, id) {
    const task = await TaskModel.findOne({ userId, _id: id });
    if (!task) {
        throw createCustomError("Task not found", 404);
    }
    return task;
}

async function update_task_by_id(userId, id, { title, description }) {
    const existingTask = await TaskModel.findOne({ userId, title });

    if (existingTask && !existingTask._id.equals(id)) {
        throw createCustomError("A task with that title already exists", 400);
    }

    const task = await TaskModel.findOne({ userId, _id: id });
    if (!task) {
        throw createCustomError("Task not found", 404);
    }
    task.title = title;
    task.description = description;
    await task.save();
    return task;
}

async function complete_task_by_id(userId, id) {
    const task = await TaskModel.findOne({ userId, _id: id });
    if (!task) {
        throw createCustomError("Task not found", 404);
    }
    task.completed = true;
    await task.save();
    return task;
}

async function delete_task_by_id(userId, id) {
    const task = await TaskModel.findOne({ userId, _id: id });
    if (!task) {
        throw createCustomError("Task not found", 404);
    }
    await TaskModel.deleteOne({ userId, _id: id });
}

module.exports = {
    create_task,
    get_all_tasks,
    get_task_by_id,
    update_task_by_id,
    complete_task_by_id,
    delete_task_by_id,
};
