const Joi = require("joi");
const { createCustomError } = require("../errors/custom-error");

const createTaskValidator = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().required().min(3),
    description: Joi.string().required(),
  });
  validateRequest(req, next, schema);
};

const updateTaskValidator = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().optional().min(3),
    description: Joi.string().optional(),
  });
  validateRequest(req, next, schema);
};

const validateRequest = (req, next, schema) => {
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true,
  };
  const { error, value } = schema.validate(req.body, options);
  if (error) {
    const errorMessage = error.details.map((error) => error.message).join(", ");
    throw createCustomError(errorMessage, 400);
  } else {
    req.body = value;
    next();
  }
};

module.exports = {
  createTaskValidator,
  updateTaskValidator,
};
