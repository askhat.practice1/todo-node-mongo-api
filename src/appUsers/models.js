const mongoose = require("mongoose");

// const dbConfig = {
//     uri: process.env.NODE_MONGO_URI || "mongodb://localhost:27017/myapp",
//     options: {
//         useNewUrlParser: true,
//         useUnifiedTopology: true,
//         useCreateIndex: true,
//     },
// };

// mongoose
//     .connect(dbConfig.uri, dbConfig.options)
//     .then(() => {
//         console.log("Connection has been established successfully.");
//     })
//     .catch((err) => {
//         console.error("Unable to connect to the database:", err);
//         process.exit(1);
//     });

const UsersSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
});

const UsersModel = mongoose.model("User", UsersSchema);

module.exports = { UsersModel };
