const UsersService = require("./services");
const { logger } = require("../utils/logger");

// register a new user
async function register_user(req, res, next) {
  try {
    const user = await UsersService.create_user(req.body);
    res.status(201).json(user);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

// login an existing user
async function login_user(req, res, next) {
  try {
    const user = await UsersService.login(req.body);
    res.json(user);
  } catch (error) {
    logger.error(error)
    next(error);
  }
}

module.exports = { register_user, login_user };
