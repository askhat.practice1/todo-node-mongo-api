const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { createCustomError } = require("../errors/custom-error");

const { UsersModel } = require("./models");

async function isUniqueEmail(email) {
  const count = await UsersModel.countDocuments({ email });
  if (count !== 0) {
    return false;
  }
  return true;
}

async function findByEmail(email) {
  const user = await UsersModel.findOne({ email });
  if (!user) {
    throw createCustomError("Email not found", 404);
  }
  return user;
}

async function findById(id) {
  const user = await UsersModel.findById(id);
  return user;
}

async function create_user({ email, password, first_name, last_name }) {
  const isUnique = await isUniqueEmail(email);
  if (!isUnique) {
    throw createCustomError("Email already exists", 400);
  }
  const user = await UsersModel.create({
    email,
    password: await bcrypt.hash(password, 10),
    first_name,
    last_name,
  });
  return user;
}

async function login({ email, password }) {
  const user = await findByEmail(email);
  const match = await bcrypt.compare(password, user.password);
  if (!match) {
    throw createCustomError("Invalid password", 400);
  }
  const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
    expiresIn: "7d",
  });
  return { token, user };
}

module.exports = { isUniqueEmail, findByEmail, findById, create_user, login };
