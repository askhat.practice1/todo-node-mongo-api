const express = require("express");
const router = express.Router();
const { logger } = require("../middleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.route("/register/").post(logger, validators.createUserValidator, controllers.register_user);
router.route("/login/").post(logger, validators.loginUserValidator, controllers.login_user);

module.exports = router;
