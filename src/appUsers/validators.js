const Joi = require("joi");
const { createCustomError } = require("../errors/custom-error");

const createUserValidator = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required().min(6),
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
  });
  validateRequest(req, next, schema);
};

const loginUserValidator = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  validateRequest(req, next, schema);
};

const validateRequest = (req, next, schema) => {
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true,
  };
  const { error, value } = schema.validate(req.body, options);
  if (error) {
    const errorMessage = error.details.map((error) => error.message).join(", ");
    throw createCustomError(errorMessage, 400);
  } else {
    req.body = value;
    next();
  }
};

module.exports = {
  createUserValidator,
  loginUserValidator,
};
