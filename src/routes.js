const express = require("express");
const router = express.Router();

const usersRouter = require("./appUsers/routes");
const tasksRouter = require("./appTasks/routes");

router.use("/users/", usersRouter);
router.use("/tasks/", tasksRouter.tasks_router);
router.use("/task/", tasksRouter.task_router);

module.exports = router;
