module.exports = {
  mongo_db_url: process.env.MONGO_URL || "mongodb://localhost:27017/myapp",
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  },
};
