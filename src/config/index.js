const databaseConfig = require("./database");
const jwtConfig = require("./jwt");
const mongoConfig = require("./mongo");

module.exports = {
  database: databaseConfig,
  jwt: jwtConfig,
  mongo: mongoConfig,
};
