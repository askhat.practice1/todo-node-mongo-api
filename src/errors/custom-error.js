/*
  Custom error class to be used in the app
  use in validators
// add in controllers ?

  usage:
const { createCustomError } = require("../errors/custom-error");
throw createCustomError('error message', 400);
// */

const { logger } = require('../utils/logger');

class CustomAPIError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
  }
}

const createCustomError = (msg, statusCode) => {
  logger.error(msg)
  return new CustomAPIError(msg, statusCode);
};

module.exports = { createCustomError, CustomAPIError };
