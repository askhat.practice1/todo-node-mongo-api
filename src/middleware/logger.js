const winstonLogger = require("../utils/logger");

async function logger(req, res, next) {
  winstonLogger.logger.info(`${req.method} ${req.originalUrl} ${req.ip}`);
  next();
}

module.exports = { logger };
