const jwt = require("jsonwebtoken");
const config = require("../config");

const authenticateUser = (req, res, next) => {
  // Get the JWT token from the request header
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];

  if (!token) {
    // If no token is found, return a 401 Unauthorized error
    return res
      .status(401)
      .json({ message: "Authentication failed: Missing token" });
  }

  try {
    // Verify the token using the secret key
    const decoded = jwt.verify(token, config.jwt.secret);
    req.user = decoded;
    next();
  } catch (error) {
    // If the token is invalid, return a 401 Unauthorized error
    return res
      .status(401)
      .json({ message: "Authentication failed: Invalid token" });
  }
};

module.exports = {
  authenticateUser,
};
