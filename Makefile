# GIT_SSH_COMMAND='ssh -i ~/.ssh/gitlab-ssh-askhat.practice1' git clone git@gitlab.com:askhat.practice1/todo-node-mongo-api.git

DOCKER_COMPOSE=docker-compose
APP=nodejs

.PHONY: all ps logs up rebuild restart stop down bash shell pull push node test format

all: ps

ps: 
	$(DOCKER_COMPOSE) ps

logs:
	$(DOCKER_COMPOSE) logs -f --tail 100

up:
	$(DOCKER_COMPOSE) up -d --build
	$(DOCKER_COMPOSE) ps

rebuild:
	$(DOCKER_COMPOSE) up -d --build --force-recreate
	$(DOCKER_COMPOSE) ps

restart:
	$(DOCKER_COMPOSE) restart
	$(DOCKER_COMPOSE) ps

stop:
	$(DOCKER_COMPOSE) stop

down:	
	$(DOCKER_COMPOSE) down

bash:
	$(DOCKER_COMPOSE) exec $(APP) bash

shell: 
	$(DOCKER_COMPOSE) exec $(APP) bash
	
pull:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git pull

push:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git push

node:
	$(DOCKER_COMPOSE) exec $(APP) sh -c "node" 

test:
	$(DOCKER_COMPOSE) run $(APP) sh -c "npm install && npm test"

format:
	npm install prettier --save-dev 
	npx prettier --write app.js src/**/*.js